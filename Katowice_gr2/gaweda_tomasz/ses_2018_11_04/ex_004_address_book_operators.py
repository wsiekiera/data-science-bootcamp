from dataclasses import field, dataclass
from typing import List


@dataclass
class Address:
    location: str = None

    def __repr__(self):
        return f'{self.__dict__}'

    def __eq__(self, other) -> bool:
        if isinstance(other, Address):
            return self.location == other.location
        return False


@dataclass
class Contact:
    name: str
    addresses: List[Address] = field(default_factory=list)

    def __iadd__(self, other):
        if isinstance(other, Address):
            self.addresses.append(other)
        else:
            raise ValueError('Wrong type of append')
        return self

    def __contains__(self, search: Address) -> bool:
        if isinstance(search, Address):
            return search in self.addresses
        return False
        # for address in self.addresses :
        #    if search == adress:
        #        return true
        # return false

    def __str__(self):
        return f'{self.__dict__}'


@dataclass
class AddressBook:
    contacts: List[Contact] = field(default_factory=list)


if __name__ == '__main__':

    contact = Contact(name='José Jiménez', addresses=[Address(location='JPL')])
    contact += Address(location='Houston')
    contact += Address(location='KSC')

    print(contact)
    # {'name': 'José Jiménez', 'addresses': [
    #       {'location': 'JPL'},
    #       {'location': 'Houston'},
    #       {'location': 'KSC'}
    # ]}

    if Address(location='Bajkonur') in contact:
        print(True)
    else:
        print(False)
    # False
