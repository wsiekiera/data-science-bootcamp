from dataclasses import dataclass, field
from pprint import pprint
from typing import List


@dataclass
class AddressBookEntryAddress:
    street: str
    city: str

    # def __str__(self):
    #    return f"Address: {self.street} - {self.city}"


@dataclass
class AddressBookEntry:
    first_name: str
    last_name: str
    addresses: List[AddressBookEntryAddress] = field(default_factory=list)

    # def __str__(self):
    #    tmp = f"First Name: {self.first_name}"
    #    tmp += f"Last Name: {self.last_name}"
    #    for addr in self.addresses:
    #        tmp += str(addr)
    #    return tmp


@dataclass
class AddressBook:
    book: List[AddressBookEntry] = field(default_factory=list)

    def append(self, flname: AddressBookEntry, addresses: List[AddressBookEntryAddress] = None):
        tmp = AddressBookEntry(flname.first_name, flname.last_name, addresses)
        self.book.append(tmp)

    # def __str__(self):
    #     tmp = f"Ksiazka adresowa:"
    #     for entry in self._book:
    #         tmp += str(entry)
    #     return tmp


if __name__ == '__main__':
    AddressBookEntry('Grzegorz', 'Brzeczyszkiewicz')

    abook = AddressBook()
    abook.append(
        AddressBookEntry("Grzegorz", "Brzeczyszkiewicz")
    )

    addr = list()
    addr.append(AddressBookEntryAddress("Dziwna ulica", "Katowice"))
    abook.append(
        AddressBookEntry("Wojtek", "Brzeczyszkiewicz"),
        addr
    )

    pprint(abook)
