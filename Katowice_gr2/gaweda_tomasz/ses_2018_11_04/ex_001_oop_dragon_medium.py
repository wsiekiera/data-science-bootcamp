from gaweda_tomasz.ses_2018_11_03.ex_005_dragon import Dragon, DragonBasicHealth


class DragonExtendedState(DragonBasicHealth):
    FULL_OF_LIFE = 'Pelnia zycia'  # 100%
    WOUNDED_LIGHT = 'Lekko ranny'  # 99% - 75%
    WOUNDED_HARD = 'Powaznie ranny'  # 75% - 25%
    NEAR_DEATH = 'Na skraju smierci'  # >=0  - 25%
    DEAD = 'dead'  # 0

    MAPPING_HP_TO_STATE = {
        # FULL_OF_LIFE: (99.09, 100.0),
        WOUNDED_LIGHT: (75.0, 99.0),
        WOUNDED_HARD: (25.0, 75.0),
        NEAR_DEATH: (1.0, 25.0),
        # DEAD: (0.0, 0.0),
    }

    @staticmethod
    def test_calc_percentage(current_hp: int, initial_hp: int) -> float:
        """
        >>> DragonExtendedState.test_calc_percentage(50, 100)
        50.0

        >>> DragonExtendedState.test_calc_percentage(50, 200)
        25.0

        :param initial_hp:
        :param current_hp:
        :return:
        """
        return DragonExtendedState._calc_percentage(current_hp, initial_hp)

    @staticmethod
    def _calc_percentage(current_hp: int, initial_hp: int) -> float:
        percentage: float = current_hp / (initial_hp * 100)
        return percentage

    def _get_state_by_health(self) -> str:

        if self.current_hp <= 0:
            return DragonExtendedState.DEAD

        if self.current_hp >= self.initial_hp:
            return DragonExtendedState.FULL_OF_LIFE

        percentage: float = DragonExtendedState._calc_percentage(self.current_hp, self.initial_hp)
        for state, bound in DragonExtendedState.MAPPING_HP_TO_STATE.items():
            if bound[0] >= percentage <= bound[1]:
                return state

    def is_alive(self) -> bool:
        return self._get_state_by_health() != DragonExtendedState.DEAD

    def is_dead(self) -> bool:
        return not self._get_state_by_health()


class ObjectPosition2dWithBoundryChecking:
    CANVAS_X_MIN = 0
    CANVAS_X_MAX = 1024
    CANVAS_Y_MIN = 0
    CANVAS_Y_MAX = 768

    def __init__(self, position_x: int, position_y: int):
        self.x = 0
        self.y = 0
        self.set_position(position_x, position_y)

    @staticmethod
    def _cut_to_boundries(x: int, mmin: int, mmax: int) -> int:
        tmp: int = x
        tmp = mmin if x <= mmin else tmp
        tmp = mmax if x >= mmax else tmp
        return tmp

    def set_position(self, x: int, y: int):
        """

        >>> pos = ObjectPosition2dWithBoundryChecking(10,0)
        >>> pos.set_position(-1, 0)
        >>> pos.get_position()
        (0, 0)

        >>> pos = ObjectPosition2dWithBoundryChecking(0,10)
        >>> pos.set_position(0, -1)
        >>> pos.get_position()
        (0, 0)

        :param x:
        :param y:
        :return:
        """

        myx = self._cut_to_boundries(x, ObjectPosition2dWithBoundryChecking.CANVAS_X_MIN,
                                     ObjectPosition2dWithBoundryChecking.CANVAS_X_MAX)
        self.x = myx

        myy = self._cut_to_boundries(y, ObjectPosition2dWithBoundryChecking.CANVAS_Y_MIN,
                                     ObjectPosition2dWithBoundryChecking.CANVAS_Y_MAX)
        self.y = myy

        pass

    def get_position(self) -> (int, int):
        return self.x, self.y


class SuperDragonWithBoundries(Dragon):
    def __init__(self, name: str, position_x: int, position_y: int):
        super().__init__(name, -1, -1)
        self.position = ObjectPosition2dWithBoundryChecking(position_x, position_y)


class Warrior(Dragon):
    START_HEALTH_MIN = 100
    START_HEALTH_MAX = 150

    DAMAGE_MAKE_MIN = 1
    DAMAGE_MAKE_MAX = 15

    GOLD_HAVE_MIN = 1
    GOLD_HAVE_MAX = 100

    def __init__(self, name: str, position_x: int, position_y: int):
        super().__init__(name, position_x, position_y)
        self.gold = 0

    def take_gold(self, ammount: int):
        self.gold += ammount


if __name__ == '__main__':
    wawelski = Dragon(name='Wawelski', position_x=50, position_y=120)
    fi = Warrior("Jose Jimenez", position_x=50, position_y=120)

    fighters_alive = True
    while fighters_alive:

        if fighters_alive:
            mk_damage: int = fi.make_damage()
            wawelski.take_damage(mk_damage)
        if wawelski.is_dead():
            fighters_alive = False

        if fighters_alive:
            mk_damage: int = wawelski.make_damage()
            fi.take_damage(mk_damage)
        if fi.is_dead():
            fighters_alive = False

        if wawelski.is_dead():
            ww_gold = wawelski.get_gold()
            fi.take_gold(ww_gold)
