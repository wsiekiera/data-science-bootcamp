# Dany jest tekst przemównienia John F. Kennedy'ego "Moon Speech"
# wygłoszony na Rice Stadium (zdania oddzielone są kropkami)
# Policz ile jest wyrazów w każdym zdaniu
# Wypisz na ekranie listę słowników (``List[Dict[str, int]]``) o strukturze:
#
#    * klucz: zdanie
#    * wartość: ilość wyrazów
#
# Na końcu wypisz także ile jest:
#
#    * zdań
#    * słów
#    * znaków
#
import re


def count_number_of_words(mystr: str) -> int:
    return len(re.split("[\s,.]+", mystr))
    # return len( sentence.split(' ') )


text = "We choose to go to the Moon. " \
       "We choose to go to the Moon in this decade and do the other things. " \
       "Not because they are easy, but because they are hard. " \
       "Because that goal will serve to organize and measure the best of our energies and skills. " \
       "Because that challenge is one that we are willing to accept. " \
       "One we are unwilling to postpone. And one we intend to win"

all_sentences_dict = {}
total_sentences = 0
total_words = 0
total_chars = 0

sentences = text.split('.')
for sentence in sentences:
    sentence = sentence.strip()
    total_chars += len(sentence)
    all_sentences_dict[sentence] = count_number_of_words(sentence)

total_sentences = len(sentences)
total_words = count_number_of_words(text)
# total_chars = len(text)

print(f" * Number of words in sentence: ")
[print(f"    * {sentence} => {number_of_words}") for sentence, number_of_words in all_sentences_dict.items()]
print(f" * Number of sentences: {total_sentences}")
print(f" * Number of words:    {total_words}")
print(f" * Number of chars:    {total_chars}")
