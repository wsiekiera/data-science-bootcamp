# Parsing ``/etc/hosts``
# ----------------------
# Do pliku ``hosts.txt`` w katalogu gdzie będzie Twój skrypt zapisz kod z szablonu:
#
# ##
# # Host Database
# #   - Unix: /etc/hosts
# #   - Windows: C:/Windows/System32/drivers/etc/hosts
# ##
#
# 127.0.0.1       localhost
# 127.0.0.1       astromatt
# 10.13.37.1      nasa.gov esa.int roscosmos.ru
# 255.255.255.255 broadcasthost
# ::1             localhost
#
# Ważne, żeby przepisać zawartość zawierającą komentarze, białe spacje i linie przerwy
# Przeglądając plik linijka po linijce sparsuj go i przedstaw w formie listy dictów jak w przykładzie poniżej:
#
#
# [
#     {'hostnames': ['localhost'], 'ip': '127.0.0.1', 'protocol': 'ipv4'},
#     {'hostnames': ['astromatt'], 'ip': '127.0.0.1', 'protocol': 'ipv4'},
#     {'hostnames': ['nasa.gov', 'esa.int', 'roscosmos.ru'], 'ip': '10.13.37.1', 'protocol': 'ipv4'},
#     {'hostnames': ['broadcasthost'], 'ip': '255.255.255.255', 'protocol': 'ipv4'},
#     {'hostnames': ['localhost'], 'ip': '::1', 'protocol': 'ipv6'}
# ]
#
# Zwróć uwagę na uprawnienia do odczytu pliku
# Wykorzystaj inline if do sprawdzenia: jeżeli jest kropka w adresie IP to IPv4 w przeciwnym przypadku IPv6
#
#
# import re
from pprint import pprint

FILENAME = r"hosts.txt"


def get_ip_version(_ip: str) -> str:
    if '.' in _ip:
        return 'IPv4'
    elif ':' in _ip:
        return 'IPv6'
    return 'Unknown'


ip_to_hostname = []
with open(FILENAME, encoding="UTF-8") as file:
    for line in file:

        if line.startswith('#') or line.isspace():
            continue
        line = line.strip()

        # content = re.split(r'[\s]+', line)  # too slow
        # content = list(filter(lambda x: True if x else False, line.split(CONST_SEPARATOR)))  # too complicated
        content = line.split()
        if len(content) <= 1:
            continue

        ip = content[0]
        hostnames = content[1:]
        ip_to_hostname.append({
            'ip': ip,
            'protocol': get_ip_version(ip),
            'hostnames': hostnames,
        })

pprint(ip_to_hostname)
