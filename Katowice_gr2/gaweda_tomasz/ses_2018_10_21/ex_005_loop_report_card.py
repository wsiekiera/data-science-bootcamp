# http://python.astrotech.io/control-flow/comprehensions.html#assignments

# Przekonwertuj skalę ocen ``(2, 3, 3.5, 4, 4.5, 5)`` na listę ``float`` za pomocą inline ``for``
#
# Użytkownik podaje oceny jako ``int`` lub ``float``
#
# Jeżeli ocena jest na liście dopuszczalnych ocen, dodaje ją do dzienniczka
#
# Jeżeli wciśnięto sam Enter, oznacza to koniec wpisywania do dzienniczka
#
# Jeżeli wpisano cyfrę nie znajdującą się na liście dopuszczalnych ocen,
# wyświetl informację "Grade is not allowed" i dalej kontynuuj wpisywanie
#
# Na zakończenie wyświetl wyliczoną dla dzienniczka średnią arytmetyczną z ocen
#
from typing import List

ALLOWED_GRADES: List[float] = [float(x) for x in [2, 3, 3.5, 4.5, 5]]
user_grade_list: List[float] = list()

while True:
    response: str = input(f"Please enter grade: ")
    if response in ("", " ", None):
        break

    user_grade: float = float(response)
    if user_grade in ALLOWED_GRADES:
        print(f"Adding grade [{user_grade}]")
        user_grade_list.append(user_grade)
    else:
        print(f"Grade is not allowed - allowed grades are [{ALLOWED_GRADES}]")

if user_grade_list:
    mysum: float = sum(user_grade_list)
    mycount: int = len(user_grade_list)
    myavg: float = mysum / float(mycount)
    print(f"Avg grade: {myavg}      number of grades: {mycount}, entered grades: {user_grade_list}")
else:
    print("Empty report card")
