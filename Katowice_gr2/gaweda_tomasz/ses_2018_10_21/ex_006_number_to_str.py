# Napisz funkcję ``aviation_numbers``
# Funkcja zamieni dowolnego ``int`` lub ``float`` na formę tekstową w mowie pilotów

from typing import Dict


def aviation_numbers(num: float) -> str:
    """
    Converts to string

    :param num:
    :return:

    >>> aviation_numbers(1969)
    'one niner six niner'

    >>> aviation_numbers(31337)
    'tree one tree tree seven'

    >>> aviation_numbers(13.37)
    'one tree and tree seven'

    >>> aviation_numbers(31.337)
    'tree one and tree tree seven'

    >>> aviation_numbers(-1969)
    'minus one niner six niner'

    >>> aviation_numbers(-31.337)
    'minus tree one and tree tree seven'

    >>> aviation_numbers(-49.35)
    'minus fower niner and tree fife'
    """
    numbers: Dict[str, str] = {
        '-': 'minus',
        '0': 'Zero',
        '1': 'One',
        '2': 'Two',
        '3': 'Tree',
        '4': 'Fower',
        '5': 'Fife',
        '6': 'Six',
        '7': 'Seven',
        '8': 'Ait',
        '9': 'Niner',
        '.': 'and',
    }
    return ' '.join([numbers.get(x, "_").lower() for x in str(num)])


if __name__ == "__main__":
    import doctest

    doctest.testmod()
