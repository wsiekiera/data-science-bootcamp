# #. Dany jest zbiór pomiarów Irysów :numref:`listing-loops-iris-sample`
# #. Ze zbioru wyodrębnij dane odrzucając nagłówek
# #. Z danych wyodrębnij:
#
#     * cechy opisujące: ``features: List[Tuple[float]]``
#     * cechy opisywane: ``labels: List[int]``
#
# #. Przykład danych wyodrębnionych:
#
#     .. code-block:: python
#
#         features = [
#             (5.8, 2.7, 5.1, 1.9),
#             (5.1, 3.5, 1.4, 0.2),
#             (5.7, 2.8, 4.1, 1.3),
#             (6.3, 2.9, 5.6, 1.8),
#             (6.4, 3.2, 4.5, 1.5),
#             (4.7, 3.2, 1.3, 0.2), ...]
#
#         labels = [0, 1, 2, 1, 2, 0, ...]
#
# # Aby móc odszyfrować ``labels`` i zamienić wartości na nazwy gatunków,
# # potrzebny jest słownik podmiany "liczba -> nazwa"
# # Wygeneruj słownik ``species: Dict[int, str]`` na podstawie danych
# # Przykład słownika ``species``:
#
#     .. code-block:: python
#
#         species = {
#             0: 'virginica',
#             1: 'setosa',
#             2: 'versicolor'
#         }
#
# #. Wyświetl na ekranie:
#
#     * ``species``
#     * ``labels``
#     * ``features``
#
# :Algorithm:
#     #. Wyodrębnij dane odrzucając nagłówek
#     #. Stwórz słownik gatunków ``species``
#     #. Iteruj po elementach zbioru danych
#     #. Gatunek to ostatni element rekordu
#     #. Jeżeli w słowniku nie ma gatunku, to dodaj go z kolejnym numerem
#     #. Do listy label dodawaj wartość słownika gatunków dla gatunku w tym rekordzie
#     #. Odwróć słownik gatunków
#     #. Wyświetl na ekranie ``species`` oraz ``labels``
from pprint import pprint

iris_dataset = [
    ('Sepal length', 'Sepal width', 'Petal length', 'Petal width', 'Species'),
    (5.8, 2.7, 5.1, 1.9, 'virginica'),
    (5.1, 3.5, 1.4, 0.2, 'setosa'),
    (5.7, 2.8, 4.1, 1.3, 'versicolor'),
    (6.3, 2.9, 5.6, 1.8, 'virginica'),
    (6.4, 3.2, 4.5, 1.5, 'versicolor'),
    (4.7, 3.2, 1.3, 0.2, 'setosa'),
    (7.0, 3.2, 4.7, 1.4, 'versicolor'),
    (7.6, 3.0, 6.6, 2.1, 'virginica'),
    (4.9, 3.0, 1.4, 0.2, 'setosa'),
    (4.9, 2.5, 4.5, 1.7, 'virginica'),
    (7.1, 3.0, 5.9, 2.1, 'virginica'),
    (4.6, 3.4, 1.4, 0.3, 'setosa'),
    (5.4, 3.9, 1.7, 0.4, 'setosa'),
    (5.7, 2.8, 4.5, 1.3, 'versicolor'),
    (5.0, 3.6, 1.4, 0.3, 'setosa'),
    (5.5, 2.3, 4.0, 1.3, 'versicolor'),
    (6.5, 3.0, 5.8, 2.2, 'virginica'),
    (6.5, 2.8, 4.6, 1.5, 'versicolor'),
    (6.3, 3.3, 6.0, 2.5, 'virginica'),
    (6.9, 3.1, 4.9, 1.5, 'versicolor'),
    (4.6, 3.1, 1.5, 0.2, 'setosa'),
]

iris_data = iris_dataset[1:]

iris_features = []
iris_labels = []
iris_species = {}
iris_species_rev = {}

for iris in iris_data:
    # get data
    current_iris_spec = iris[-1]
    current_iris_feat = iris[:-1]

    # remap it
    idx = iris_species.get(current_iris_spec, None)
    if idx is None:
        idx = len(iris_species.keys())
        iris_species[current_iris_spec] = idx
        iris_species_rev[idx] = current_iris_spec

    # store it
    iris_labels.append(idx)
    iris_features.append(current_iris_feat)

# iris_species_rev = {v: k for k, v in iris_species.items()}

print("OUT: SPECIES")
pprint(iris_species)
pprint(iris_species_rev)

print("OUT: FEATURES")
pprint(iris_features)

print("OUT: LABELS")
pprint(iris_labels)
