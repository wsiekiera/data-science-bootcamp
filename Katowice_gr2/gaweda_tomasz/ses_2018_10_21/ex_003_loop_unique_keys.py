# . Mając bazę danych z listingu poniżej
# . Wygeneruj listę unikalnych kluczy dictów

# . Iteruj po rekordach w bazie danych
# . Z rekordu wyciągnij klucze
# . Dodaj klucze do zbioru
# . Usuń duplikaty w zbiorze
from pprint import pprint

DATABASE = [
    {'last_name': 'Jiménez'},
    {'first_name': 'Mark', 'last_name': 'Watney'},
    {'first_name': 'Иван', 'age': 42},
    {'first_name': 'Matt', 'last_name': 'Kowalski', 'born': 1961},
    {'first_name': 'José', 'born': 1961, 'agency': 'NASA'},
]

unique_key_names = set()

for item in DATABASE:
    unique_key_names.update(set(item.keys()))

pprint(unique_key_names)
