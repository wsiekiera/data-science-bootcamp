# Napisz program, który poprosi użytkownika o wiek
# Użytkownik będzie podawał tylko i wyłącznie ``int`` lub ``float``
# Następnie sprawdzi pełnoletność i wyświetli informację czy osoba jest "dorosła" czy "niepełnoletnia"


ADULT_AGE = 18

user_age = float(input("Podaj wiek: "))

if user_age >= ADULT_AGE:
    print("You are adult!")
elif user_age <= 0.0:
    print("You are not event born")
else:
    print("You are **NOT** adult")
