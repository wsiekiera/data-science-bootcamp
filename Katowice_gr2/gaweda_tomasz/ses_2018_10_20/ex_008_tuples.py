# http://python.astrotech.io/data-types/sequences.html#assignments

# . Stwórz ``tuple`` z cyframi 0, 1, 2, 3
# . Przekonwertuj ją do ``list``
# . Na pierwsze miejsce w liście dodaj całą oryginalną ``tuple``
# . Przekonwertuj wszystko na płaski ``set`` unikalnych wartości wykorzystując ``slice``


from pprint import pprint

mtuple = tuple(range(0, 10))

mlist = list(mtuple)
mlist.insert(0, mtuple)

mset = set(mlist[0])
mset.update(mlist[1:])

pprint(locals())
