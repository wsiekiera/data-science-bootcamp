import scipy.constants as myconst

# input
dist_in_meters: int = 31337

# computations
dist_in_km_rounded = int(dist_in_meters / 1000.0)
dist_in_us_miles = dist_in_meters / myconst.mile
dist_in_nautical_miles = dist_in_meters / myconst.nautical_mile

# print
print(f'Meters: {dist_in_meters}')  # int
print(f'Kilometers: {dist_in_km_rounded}')  # int
print(f'Miles: {dist_in_us_miles}')  # float
print(f'Nautical Miles: {dist_in_nautical_miles}')  # float
print(f'All: {dist_in_meters}, {dist_in_meters}, {dist_in_us_miles}, {dist_in_nautical_miles}')
