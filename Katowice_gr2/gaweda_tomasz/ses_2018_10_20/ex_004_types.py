# Types casting
# http://python.astrotech.io/data-types/numeric.html

# Ineteger
a = int(10)
b = int(10.0)
c = int("10")
d = int(10.5)  # no error - precision lost
e = int("10.5")  # should be float()

print([a, b, c, d, e])
