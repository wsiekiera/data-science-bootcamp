# http://python.astrotech.io/quality-and-tests/doctest.html
# How to use doctest and doc testing

# używane jako zawsze aktualna dokumentacja kodu
# sprawdzają poprawność działania funkcji
# niezwykle przydatne przy tworzeniu regexpów
# można przetykać tekst pomiędzy testami


def add(a, b):
    """"
    Sums two numbers

    >>> add(1,3)
    4
    """
    return a + b


if __name__ == "__main__":
    import doctest

    doctest.testmod()
