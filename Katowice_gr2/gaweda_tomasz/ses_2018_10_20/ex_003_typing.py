# http://python.astrotech.io/quality-and-tests/type-annotation.html

from typing import List, Union, Optional


def add_001(a) -> float:
    return sum(a)


def add_002(a: List[int]) -> float:
    return sum(a)


def add_003(a: List[Union[int, float]]) -> float:
    return sum(a)


def add_004(a: Optional[List[Union[int, float]]]) -> float:
    return sum(a)


one_milion = 1e6
print(f"one_milion = 1e6 ==> {one_milion}")
one_milion += 1_000_000
print(f"one_milion = 1e6 + 1_000_000 ==> {one_milion}")
