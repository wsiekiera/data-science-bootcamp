import re
from pprint import pprint

a = 'Jana III Sobieskiego 1 apt 2'
b = 'ul Jana III SobIESkiego 1/2'
c = 'ul. Jana trzeciego Sobieskiego 1/2'
d = 'ul.Jana III Sobieskiego 1/2'
e = 'ulicaJana III Sobieskiego 1/2'
f = 'UL. JANA 3 SOBIESKIEGO 1/2'
g = 'UL. III SOBiesKIEGO 1/2'
h = 'ULICA JANA III SOBIESKIEGO 1/2'
i = 'ULICA. JANA III SOBI'
j = ' Jana 3 Sobieskiego 1/2 '
k = 'Jana III Sobieskiego 1 m. 2'
z = ' 1/2'

# Bleh

mylist = [a, b, c, d, e, f, g, h, i, j, k, z]
mylist_orig = mylist.copy()

print("BEFORE: " + str(mylist))
mylist = list(map(lambda x: "Jana III Sobieskiego", mylist))
print("AFTER:" + str(mylist))


# FUN

def clean_ul(ul: str) -> str:
    return re.sub("^\s*(ulica|ul)[\. ]{0,2}", "", ul, flags=re.I)


def clean_num(ul: str) -> str:
    return re.sub("\s*\d+\s*(/|m\.|apt)\s*\d+$", "", ul, flags=re.I)


def clean_name(ul: str) -> str:
    ul = re.sub("Iii", "III", ul)
    ul = re.sub("\s+3\s+", "III", ul)
    ul = re.sub("Trzeciego", "III", ul)
    ul = re.sub("naIIISo", "na III So", ul)
    ul = re.sub("^III", "Jana III", ul)
    ul = re.sub("Sobi$", "Sobieskiego", ul)
    return ul


mylist = mylist_orig.copy()
print("BEFORE: " + str(mylist))
mylist = list(map(lambda x: x.strip(), mylist))
mylist = list(map(lambda x: clean_ul(x), mylist))
mylist = list(map(lambda x: clean_num(x), mylist))
mylist = list(map(lambda x: x.title(), mylist))
mylist = list(map(lambda x: clean_name(x), mylist))
print("AFTER:" + str(mylist))

# Desired solution

a = a.replace(" 1 apt 2", "")
# b = ...
# c = ...
pprint(locals())
