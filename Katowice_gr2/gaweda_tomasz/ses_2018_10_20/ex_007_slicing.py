from pprint import pprint

text = 'We choose to go to the Moon!'
print(text[23:28])
print(text[-5:-1])
print(text[0:0])
print(text[3:-5])

# . Z podanych poniżej ciągów znaków
# . Za pomocą ``[...]`` wydobądź ``Jana III Sobieskiego``
# . Jakie parametry użyłeś dla każdej z linijek?


a = 'UL. Jana III Sobieskiego 1/2'
b = 'ulica Jana III Sobieskiego 1 apt 2'
c = 'os. Jana III Sobieskiego'
d = 'plac Jana III Sobieskiego 1/2'
h = 'os. Jana III Sobieskiego 1 apt 2'
e = 'aleja Jana III Sobieskiego'
f = 'alei Jana III Sobieskiego 1/2'
g = 'Jana III Sobieskiego 1 m. 2'

(a, c, h) = map(lambda x: x[4:], [a, c, h])
(b, e) = map(lambda x: x[6:], [b, e])
(d, f) = map(lambda x: x[5:], [d, f])

(a, d, f) = map(lambda x: x[:-4], [a, d, f])
(b, h, g) = map(lambda x: x[:-8], [b, h, g])

pprint(locals())
