#
# Za pomocą regexpów wytnij tekst fragmentu przemówienia JFK
# Zwróć pierwszy paragraf tekstu przemówienia zaczynający się od słów “We choose to go to the moon”
#
# wget https://er.jsc.nasa.gov/seh/ricetalk.htm
#

import re
import textwrap

FILE_NAME="ricetalk.htm"
with open(FILE_NAME, encoding="cp1250") as file:
    whole_text = file.read()

    text = re.findall(r"(We choose to go to the moon(?:.+\n)+?)</?p>", whole_text, flags=re.MULTILINE)
    text = re.sub(r"[\r\n]+", " ", text[0], flags=re.MULTILINE)
    eighty_lines = textwrap.wrap(text, width=80)

    print("\n\n" + "\n".join(eighty_lines))




