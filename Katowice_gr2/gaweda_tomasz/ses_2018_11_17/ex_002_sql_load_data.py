import datetime
import sqlite3

FILENAME = 'iris.csv'
DATABASE = 'db_iris.sqlite3'

SQL_CREATE_TABLE = """
    CREATE TABLE IF NOT EXISTS iris (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        datetime DATETIME,
        species TEXT,
        sepal_length REAL,
        sepal_width REAL,
        petal_length REAL,
        petal_width REAL
    );
"""

SQL_CREATE_INDEX = "CREATE INDEX IF NOT EXISTS datetime_index ON iris (datetime);"

SQL_INSERT = """
    INSERT INTO iris VALUES (
        NULL,
        :datetime,
        :species,
        :sepal_length,
        :sepal_width,
        :petal_length,
        :petal_width
    )
"""

SQL_DELETE = 'DELETE FROM iris'
SQL_SELECT = 'SELECT * FROM iris ORDER BY datetime DESC'

with open(FILENAME, encoding='utf-8', mode='r') as iris_file:
    count_all, count_params, *species = iris_file.readline().strip().split(',')

    with sqlite3.connect(DATABASE) as db:
        db.row_factory = sqlite3.Row
        db.execute(SQL_CREATE_TABLE)
        rows: int = db.execute(SQL_DELETE).rowcount
        db.commit()
        print(f'Rows deleted {rows}')

        all_data = []
        for line in iris_file:
            row = line.split(',')
            data = {
                'datetime': datetime.datetime.utcnow(),
                'sepal_length': float(row[0]),
                'sepal_width': float(row[1]),
                'petal_length': float(row[2]),
                'petal_width': float(row[3]),
                'species': species[int(row[4])],
            }
            all_data.append(data)
        db.executemany(SQL_INSERT, all_data)
        db.execute(SQL_CREATE_INDEX)

with sqlite3.connect(DATABASE) as db:
    for row in db.execute(SQL_SELECT):
        print(row)

with sqlite3.connect(DATABASE) as db:
    db.row_factory = sqlite3.Row
    for row in db.execute(SQL_SELECT):
        print(dict(row))
