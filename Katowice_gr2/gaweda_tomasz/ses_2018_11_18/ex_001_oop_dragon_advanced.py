from gaweda_tomasz.ses_2018_11_03.ex_005_dragon import Dragon, DragonBasicHealth
from gaweda_tomasz.ses_2018_11_04.ex_001_oop_dragon_medium import DragonExtendedState, \
    ObjectPosition2dWithBoundryChecking, SuperDragonWithBoundries, Warrior


class ObjectPosition3dWithBoundryChecking(ObjectPosition2dWithBoundryChecking):
    CANVAS_X_MIN = 0
    CANVAS_X_MAX = 1024
    CANVAS_Y_MIN = 0
    CANVAS_Y_MAX = 768
    CANVAS_Z_MIN = 0
    CANVAS_Z_MAX = 100

    def __init__(self, position_x: int, position_y: int, position_z: int):
        self.x = 0

    self.y = 0
    self.z = 0
    self.set_position(position_x, position_y, position_z)

    def set_position(self, x: int, y: int, y: int):
        """

        >>> pos = ObjectPosition2dWithBoundryChecking(10,0,30)
        >>> pos.set_position(-1, 0,30)
        >>> pos.get_position()
        (0, 0, 30)

        >>> pos = ObjectPosition2dWithBoundryChecking(0,10,10)
        >>> pos.set_position(0, -1, 30)
        >>> pos.get_position()
        (0, 0, 30)

        :param x:
        :param y:
        :return:
        """

        myx = self._cut_to_boundries(x, self.CANVAS_X_MIN, self.CANVAS_X_MAX)
        self.x = myx

        myy = self._cut_to_boundries(y, self.CANVAS_Y_MIN, self.CANVAS_Y_MAX)
        self.y = myy

        myz = self._cut_to_boundries(z, self.CANVAS_Z_MIN, self.CANVAS_Z_MAX)
        self.y = myy

        pass

    def get_position(self) -> (int, int, int):
        return self.x, self.y, self.z


class