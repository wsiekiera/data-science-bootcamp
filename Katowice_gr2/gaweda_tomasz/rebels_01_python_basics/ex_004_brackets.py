BRACKET_PAIRS = (
    ('(', ')'),
    ('[', ']'),
    ('{', '}'),
)


def brackets(input_string: str) -> bool:
    """
    Sprawdz czy nawiasy podane w stringu są poprawne. Przykłady:
    >>> brackets("()[]([])")
    True
    >>> brackets("[(])")
    False
    >>> brackets("[[[((()))}}}[](((())))")
    False
    >>> brackets("[[[((()))]]][](((())))")
    True
    >>> brackets("[A[[((()))]]][](((())))")
    False
    """
    brackets_close_map = {}
    stack: [str] = list()

    for (idx, (bopen, bclose)) in enumerate(BRACKET_PAIRS):
        brackets_close_map[bclose] = bopen

    for char in input_string:
        if char not in brackets_close_map:
            stack.append(char)
            # print(f"Pushed to stack - {char}")
        elif char in brackets_close_map:
            mchar = stack.pop()
            # print(f"Popped from stack - {mchar} - {char}")
            if brackets_close_map[char] != mchar:
                return False

    if len(stack) > 0:
        return False

    return True
