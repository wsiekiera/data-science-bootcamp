"""
Zwróc najwyżej punktowane słowo z podanych. Każdej literze przyporządkowana jest liczba punktów w nastepujący sposób:
a=1, b=2, c=3, d=4 itd
Funkcja na wejściu dostaje listę stringów. Ma zwrócić listę zawierającą stringi dla których suma punktów
dla poszczególnych liter jest największa. Przykładowo:
   >>> highest_score(["abc","def","ghi"])
   ['ghi']

dlatego, że:
"abc" = 1+2+3 = 6
"def" = 4 + 5 + 6 = 15
"ghi" = 7 + 8 + 9 = 24

Na wejściu nie będzie polskich znaków. Gdy jest więcej niż 1 słowo z max punktacją na liście - zwracamy wszystkie.
Duże i małe litery punktujemy tak samo.
"""


def char_range(c1: str, c2: str) -> str:
    """Generates the characters from `c1` to `c2`, inclusive."""
    for c in range(ord(c1), ord(c2) + 1):
        yield chr(c)


def highest_score(words: []) -> [str]:
    """
    >>> highest_score(["abc","def","ghi"])
    ['ghi']

    >>> highest_score(["aa", "bb", "cc"])
    ['cc']

    >>> highest_score(["abc"])
    ['abc']

    >>> highest_score(["aaa", "c", "b", "ba"])
    ['aaa', 'c', 'ba']

    """
    results: [str] = list()
    if len(words) == 0:
        return results

    char2point = {}
    for (char_point_value, char) in enumerate(char_range('a', 'z'), 1):
        char_lower = char.lower()
        char_upper = char.upper()

        char2point[char_lower] = char_point_value
        char2point[char_upper] = char_point_value

    max_word_points = 0
    all_word_points: [int] = list()
    for (idx, word) in enumerate(words):
        word_point_value = sum(map(lambda x: char2point.get(x, 0), word))
        max_word_points = max(max_word_points, word_point_value)
        all_word_points.insert(idx, word_point_value)

    for (idx, word_point_value) in enumerate(all_word_points):
        if word_point_value >= max_word_points:
            results.append(words[idx])

    return results
