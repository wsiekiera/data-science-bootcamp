# https://stackoverflow.com/questions/40966319/parsing-program-input-string-for-math-expression


# Simple Shunting-Yard solution
#
# Given a math expression, parse and evaluate
# the expression
#
# E.g '2+1' => 3, 8/2*4+1 => 17, 2+(1*2) = 4, ((2+4)/2*7) => 21
#

# http://rpnevaluator.andreasandersen.dk/default.aspx
def parse_math_expression(exp):
    """
    nie dziala parsowanie nawiasow
    RPN nie jest wcale takie proste ;)

    >>> parse_math_expression("2+1")
    ['2', '1', '+']

    #>>> parse_math_expression("((2+4)/(2*7))")
    #['2', '4', '+', '2', '7', '*', '/']

    >>> parse_math_expression("3*2+4/2")
    ['3', '2', '*', '4', '2', '/', '+']

    >>> parse_math_expression("3+5*8+7+2*7/2")
    ['3', '5', '8', '*', '+', '7', '+', '2', '7', '*', '2', '/', '+']

    >>> parse_math_expression("1+2*3-4")
    ['1', '2', '3', '*', '+', '4', '-']

    """
    precendence = {
        '(': 9,
        '*': 3,
        '/': 3,
        '+': 2,
        '-': 2,
        ')': 0,
    }
    output = []
    operators = []
    for ch in exp:
        if ch.isdigit():
            output.append(ch)
        elif ch in precendence:
            top_op = len(operators) and operators[0]
            while top_op and precendence[top_op] >= precendence.get(ch, -1):
                # print(f"TOP OP: {top_op}")
                x = operators.pop(0)
                if x not in ['(', ')']:
                    output.append(x)
                top_op = len(operators) and operators[0]
            operators.insert(0, ch)
        else:
            print(f"Not recognized character: {ch}")
        # print(f"OUTPUT[] - {ch} - SIZE: {len(output)} - OUTPUT: {output}")
        # print(f"OPERATORS[] - {ch} - SIZE: {len(operators)} - STACK: {operators}")
    # Handle any leftover operators
    while len(operators):
        x = operators.pop(0)
        if x not in ['(', ')']:
            output.append(x)
    return output


def eval_parsed_expression(expression):
    """
    >>> eval_parsed_expression(["3", "5", "8", "*", "+", "7", "+", "2", "7", "*", "2", "/", "+"])
    57

    >>> eval_parsed_expression(['2', '1', '+']) # (2+1)
    3

    >>> eval_parsed_expression(['2', '1', '2', '*', '+']) # (2+1*2)
    4

    >>> eval_parsed_expression(['3', '2', '*', '4', '2', '/', '+']) # (3*2)+(4/2)
    8

    :param expression:
    :return:
    """
    operators = {
        '+': lambda a, b: a + b,
        '-': lambda a, b: a - b,
        '*': lambda a, b: a * b,
        '/': lambda a, b: a / b
    }
    tmp = []
    while len(expression) > 1:
        possible_oper = expression.pop(0)
        while possible_oper not in operators:
            tmp.insert(0, possible_oper)
            possible_oper = expression.pop(0)
        operator = possible_oper
        operand_b = tmp.pop(0)
        operand_a = tmp.pop(0)
        result = operators[operator](int(operand_a), int(operand_b))
        expression.insert(0, result)
    return expression[0]


def calculator(equation: str) -> int:
    """
    Napisz prostą funkcję wykonującą operacje + - * / (pamiętaj o kolejności działań!).
    Na wejściu jest string z kompletnym wyrażeniem. Przykłady:
    >>> calculator("2+2*2")
    6
    >>> calculator("3+5*8+7+2*7/2")
    57
    """
    rpn: [str] = parse_math_expression(equation)
    # print("".join(rpn))
    solution = eval_parsed_expression(rpn)
    return solution
