from typing import List


def invert_binary(input_array: List[int]) -> List[int]:
    """
    >>> invert_binary([0, 0, 0])
    [1, 1, 1]
    >>> invert_binary([0, 0, 1, 1])
    [1, 1, 0, 0]
    >>> invert_binary([0, 0, 1, 1, 0])
    [1, 1, 0, 0, 1]
    """
    output = []
    for item in input_array:
        output.append({1: 0, 0: 1}.get(item, None))

    return output
