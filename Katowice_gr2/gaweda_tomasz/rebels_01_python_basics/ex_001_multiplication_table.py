from typing import List


def multiplication_table(width: int, height: int) -> List[List[int]]:
    """

    >>> multiplication_table(4,4)
    [[1, 2, 3, 4], [2, 4, 6, 8], [3, 6, 9, 12], [4, 8, 12, 16]]

    >>> multiplication_table(1, 2)
    [[1], [2]]

    >>> multiplication_table(3, 4)
    [[1, 2, 3], [2, 4, 6], [3, 6, 9], [4, 8, 12]]

    """
    mtable = []
    for cheight in range(1, height + 1):
        mtable.append(list(range(cheight, cheight * (width + 1), cheight)))
    return mtable


def multiplication_table_print(width: int, height: int) -> None:
    """

    >>> multiplication_table_print(4,4)
    [1, 2, 3, 4]
    [2, 4, 6, 8]
    [3, 6, 9, 12]
    [4, 8, 12, 16]

    >>> multiplication_table_print(1, 2)
    [1]
    [2]

    >>> multiplication_table_print(3, 4)
    [1, 2, 3]
    [2, 4, 6]
    [3, 6, 9]
    [4, 8, 12]

    """
    for cheight in range(1, height + 1):
        print(list(range(cheight, cheight * (width + 1), cheight)))


if __name__ == "__main__":
    import doctest

    doctest.testmod()
