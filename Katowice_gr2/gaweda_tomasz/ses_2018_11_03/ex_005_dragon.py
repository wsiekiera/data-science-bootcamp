from random import randint
from enum import Enum


class DragonBasicHealth:
    ALIVE = 'alive'
    DEAD = 'dead'

    def __init__(self, mmin: int = 0, mmax: int = 0):
        health = randint(mmin, mmax)
        self.current_hp = health
        self.initial_hp = health
        self.state = self._get_state_by_health()

    def _get_state_by_health(self) -> str:
        if self.current_hp <= 0:
            return DragonBasicHealth.DEAD
        return DragonBasicHealth.ALIVE

    def decrease(self, damage: int) -> int:
        self.current_hp -= damage
        self.state = self._get_state_by_health()
        return self.current_hp

    def get_current_health(self) -> int:
        return self.current_hp

    def get_initial_health(self) -> int:
        return self.initial_hp

    def is_alive(self) -> bool:
        return self._get_state_by_health() != DragonBasicHealth.DEAD

    def is_dead(self) -> bool:
        return not self.is_alive()


class DragonTexture(Enum):
    DEFAULT = 'dragon.png'
    DEAD = 'dragon-dead.png'


class ObjectPosition2d:
    def __init__(self, position_x: int, position_y: int):
        self.x = position_x
        self.y = position_y

    def set_position(self, x: int, y: int):
        self.x = x
        self.y = y

    def get_position(self) -> (int, int):
        return self.x, self.y

    def move(self, left: int = 0, right: int = 0, up: int = 0, down: int = 0):
        for pos in (left, right, up, down):
            if pos < 0:
                raise ValueError(f"Wrong argument: {pos}")

        self.set_position(
            x=self.x + right - left,
            y=self.y + up + down
        )


class Dragon:
    START_HEALTH_MIN = 50
    START_HEALTH_MAX = 100

    DAMAGE_MAKE_MIN = 5
    DAMAGE_MAKE_MAX = 20

    GOLD_HAVE_MIN = 1
    GOLD_HAVE_MAX = 100

    def __init__(self, name: str, position_x: int, position_y: int, texture: str = DragonTexture.DEFAULT):
        self.name = name
        self.texture = texture
        self.gold = randint(self.GOLD_HAVE_MIN, self.GOLD_HAVE_MAX)

        self.position = ObjectPosition2d(position_x, position_y)
        self.health = DragonBasicHealth(Dragon.START_HEALTH_MIN, Dragon.START_HEALTH_MAX)

    def set_position(self, x: int, y: int):
        self.position.set_position(x, y)

    def get_position(self):
        return self.position.get_position()

    def move(self, left: int = 0, right: int = 0, up: int = 0, down: int = 0):
        self.position.move(left, right, up, down)

    def is_alive(self):
        return self.health.is_alive()

    def is_dead(self):
        return self.health.is_dead()

    def _action_died(self):
        self.texture = DragonTexture.DEAD
        print(f"{self.name} is dead")
        print(f"Position of death: { self.get_position() }")
        print(f"Gold from {self.name}: {self.gold}")

    def make_damage(self):
        return randint(self.DAMAGE_MAKE_MIN, self.DAMAGE_MAKE_MAX)

    def take_damage(self, damage: int):

        if damage is None or damage < 0:
            return
            # raise ValueError(f"Wrong argument damage: {damage}")

        if self.health.is_dead():
            return
            # raise ValueError("Drogon already died")

        self.health.decrease(damage)
        print(f"N: {self.name}, Health: {self.health.get_current_health()}, TookDamage: {damage}")

        if self.health.is_dead():
            self._action_died()

    def get_gold(self) -> int:
        return self.gold


def main():
    wawelski = Dragon(name='Wawelski', position_x=50, position_y=120)

    wawelski.set_position(x=100, y=20)
    wawelski.move(left=10, down=20)
    wawelski.move(left=10, right=15)
    wawelski.move(right=15, up=5)
    wawelski.move(down=5)

    wawelski.take_damage(10)
    wawelski.take_damage(50)
    wawelski.take_damage(35)
    wawelski.take_damage(20)
    wawelski.take_damage(25)


if __name__ == '__main__':
    main()
