import random
from collections import Counter
from random import shuffle

a = [1, 2, 3, 4]

print(a)
b = shuffle(a)

print(a)
print(b)

random_numbers = [random.randint(0, 10) for a in range(0, 50)]
counter = Counter(random_numbers)

counter.most_common(5)
# [(7, 12), (4, 8), (9, 6), (1, 5), (2, 4)]
