import numpy as np


def matrix_multiplication_np_001(a, b):
    """
    >>> a = [[1, 0], [0, 1]]
    >>> b = [[4, 1], [2, 2]]
    >>> matrix_multiplication(a, b)
    [[4, 1], [2, 2]]

    >>> a = [[1,0,1,0], [0,1,1,0], [3,2,1,0], [4,1,2,0]]
    >>> b = [[4,1], [2,2], [5,1], [2,3]]
    >>> matrix_multiplication(a, b)
    [[9, 2], [7, 3], [21, 8], [28, 8]]
    """
    # solution 1
    a = np.matrix(a)
    b = np.matrix(b)
    return a * b

def matrix_multiplication_np_002(a, b):
        """
        >>> a = [[1, 0], [0, 1]]
        >>> b = [[4, 1], [2, 2]]
        >>> matrix_multiplication(a, b)
        [[4, 1], [2, 2]]

        >>> a = [[1,0,1,0], [0,1,1,0], [3,2,1,0], [4,1,2,0]]
        >>> b = [[4,1], [2,2], [5,1], [2,3]]
        >>> matrix_multiplication(a, b)
        [[9, 2], [7, 3], [21, 8], [28, 8]]
        """
        # solution 2
        return a @ b


def matrix_multiplication_np_003(a, b):
    """
    >>> a = [[1, 0], [0, 1]]
    >>> b = [[4, 1], [2, 2]]
    >>> matrix_multiplication(a, b)
    [[4, 1], [2, 2]]

    >>> a = [[1,0,1,0], [0,1,1,0], [3,2,1,0], [4,1,2,0]]
    >>> b = [[4,1], [2,2], [5,1], [2,3]]
    >>> matrix_multiplication(a, b)
    [[9, 2], [7, 3], [21, 8], [28, 8]]
    """
    # solution 3
    return np.matmul(a, b)



def matrix_multiplication(a, b):
    """
    >>> a = [[1, 0], [0, 1]]
    >>> b = [[4, 1], [2, 2]]
    >>> matrix_multiplication(a, b)
    [[4, 1], [2, 2]]

    >>> a = [[1,0,1,0], [0,1,1,0], [3,2,1,0], [4,1,2,0]]
    >>> b = [[4,1], [2,2], [5,1], [2,3]]
    >>> matrix_multiplication(a, b)
    [[9, 2], [7, 3], [21, 8], [28, 8]]
    """

    # solution 4

    result = []
    n_rows_a = len(a)
    n_rows_b = len(b)
    n_cols_b = len(b[0])

    for i in range(n_rows_a):
        row = []

        for j in range(n_cols_b):
            total = 0

            for k in range(n_rows_b):
                total += a[i][k] * b[k][j]

            row.append(total)
        result.append(row)

    return result
