from math import sqrt


def euclidean_distance_n_dimensions(a, b):
    """
    >>> a = (0,1,0,1)
    >>> b = (1,1,0,0)
    >>> euclidean_distance_n_dimensions(a, b)
    1.4142135623730951

    >>> euclidean_distance_n_dimensions((0,0,0), (0,0,0))
    0.0

    >>> euclidean_distance_n_dimensions((0,0,0), (1,1,1))
    1.7320508075688772

    >>> euclidean_distance_n_dimensions((0,1,0,1), (1,1,0,0))
    1.4142135623730951

    >>> euclidean_distance_n_dimensions((0,0,1,0,1), (1,1,0,0,1))
    1.7320508075688772

    >>> euclidean_distance_n_dimensions((0,0,1,0,1), (1,1))
    Traceback (most recent call last):
        ...
    ValueError: Punkty muszą być w przestrzeni tylu-samo wymiarowej
    """
    if len(a) != len(a):
        raise ValueError("Punkty muszą być w przestrzeni tylu-samo wymiarowej")

    # mysum: float = 0.0
    # for i in range(0, len(A)):
    #     mysum += pow(A[i] - B[i], 2)

    mysum: float = 0.0
    for i, _e in enumerate(a):
        mysum += pow(a[i] - b[i], 2)

    return sqrt(mysum)
