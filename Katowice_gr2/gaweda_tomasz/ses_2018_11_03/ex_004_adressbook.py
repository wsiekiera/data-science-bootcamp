from typing import List


class AddressBookEntryAddress:
    def __init__(self, street: str = "", city: str = ""):
        # self.address = content
        self.street = street
        self.city = city
        pass

    def __str__(self):
        return f"Address: {self.street} - {self.city}"


class AddressBookEntry:
    def __init__(self, first_name: str, last_name: str, addresses: List[AddressBookEntryAddress] = None):
        self.first_name = first_name
        self.last_name = last_name
        self.addresses = addresses if addresses else list()
        pass

    def __str__(self):
        tmp = f"First Name: {self.first_name}"
        tmp += f"Last Name: {self.last_name}"
        for tmpaddr in self.addresses:
            tmp += str(tmpaddr)
        return tmp


class AddressBook:
    def __init__(self):
        self._book = list()
        pass

    def append(self, flname: AddressBookEntry, addresses: List[AddressBookEntryAddress] = None):
        tmp = AddressBookEntry(flname.first_name, flname.last_name, addresses)
        self._book.append(tmp)

    def __str__(self):
        tmp = f"Ksiazka adresowa:"
        for entry in self._book:
            tmp += str(entry)
        return tmp


AddressBookEntry('Grzegorz', 'Brzeczyszkiewicz')

abook = AddressBook()
abook.append(
    AddressBookEntry("Grzegorz", "Brzeczyszkiewicz")
)

addr = list()
addr.append(AddressBookEntryAddress("Dziwna ulica", "Katowice"))
abook.append(
    AddressBookEntry("Wojtek", "Brzeczyszkiewicz"),
    addr
)

print(abook)

# class Contact:
#     pass
#
# class Address:
#     pass
#
#
# melissa = Contact(imie='Melissa', nazwisko='Lewis')
# print(melissa)
# # Melissa Lewis
#
# mark = Contact(imie='Mark', nazwisko='Watney', adresy=[Address(miasto='Houston'), Address(miasto='Cocoa Beach')])
# print(mark)
# # Mark Watney [Houston, Cocoa Beach]
#
# addressbook = [
#     Contact(imie='Matt', nazwisko='Kowalski', adresy=[
#         Address(ulica='2101 E NASA Pkwy', miasto='Houston', stan='Texas', kod='77058', panstwo='USA'),
#         Address(ulica=None, miasto='Kennedy Space Center', kod='32899', panstwo='USA'),
#         Address(ulica='4800 Oak Grove Dr', miasto='Pasadena', kod='91109', panstwo='USA'),
#         Address(ulica='2825 E Ave P', miasto='Palmdale', stan='California', kod='93550', panstwo='USA'),
#     ]),
#     Contact(imie='José', nazwisko='Jiménez'),
#     Contact(imie='Иван', nazwisko='Иванович', adresy=[]),
# ]
#
#
# print(addressbook)
# # [Matt Kowalski [Houston, Kennedy Space Center, Pasadena, Palmdale], José Jiménez, Иван Иванович]
