iterable = ['spring', 'summer', 'autumn', 'winter']

iterator = iter(iterable)

try:
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
except StopIteration:
    print("--KONIEC--")


# milion_squares = (x * x for x in range(1, 100_000_001))
# print(milion_squares)
# print("\n".join(list(milion_squares)))


def gen123():
    yield 1
    yield 2
    print('hehe 1')
    yield 3
    print("he he hue 2")
    yield 4
    print("jol jol mdfka")


g = gen123()
print(type(g))

try:
    print(next(g))
    print(next(g))
    print(next(g))
    print(next(g))
    print(next(g))
except StopIteration:
    print("--KONIEC--")
