def sentence(name):
    name_upper = name.upper()

    def show_sentence():
        return f"Mam na imie: {name_upper}"

    return show_sentence


sen1 = sentence("Pawel");
print(sen1())
