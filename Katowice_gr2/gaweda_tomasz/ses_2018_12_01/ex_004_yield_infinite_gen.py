def inf_gen():
    i = -1
    while True:
        i += 1
        yield i
        if i > 100:
            return
            #raise StopIteration


for i in inf_gen():
    print(i)
