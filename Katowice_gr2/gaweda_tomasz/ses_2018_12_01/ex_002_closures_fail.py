# def gen(liste):
#     i = 0
#     def next():
#         element = liste[i]
#         i += 1
#         return element
#
#     return next


def gen(liste):
    i = 0

    def next():
        nonlocal i
        element = liste[i]
        i += 1
        return element

    return next


gen1 = gen(["aa", "bb", "cc"])

for a in [gen1(), gen1(), gen1()]:
    print(a)

# print(gen1())
# print(gen1())
# print(gen1())
# print(gen1())
# print(gen1())
