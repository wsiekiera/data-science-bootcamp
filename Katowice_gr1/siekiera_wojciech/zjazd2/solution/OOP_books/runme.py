from siekiera_wojciech.zjazd2.OOP_books_solution.author import Author
from siekiera_wojciech.zjazd2.OOP_books_solution.book import Book
from datetime import datetime

from siekiera_wojciech.zjazd2.OOP_books_solution.customer import Customer

book = Book("Sample title", [], 12.5, 1999, 10)
author = Author("Tester", "Testowski", datetime.strptime('2018-04-10', '%Y-%m-%d'), [])
customer = Customer("Klient", "Kliencki", datetime.strptime('2018-04-10', '%Y-%m-%d'))

author.add_book(book)
customer.add_book(book, 3)
