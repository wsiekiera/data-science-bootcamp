class Book:
    def __init__(self, title: str, authors: [], price: float, year: int, items_number: int):
        self.title = title
        self.authors = authors
        self.price = price
        self.year = year
        self.items_number = items_number

    def add_author(self, author):
        self.authors.append(author)

    def delete_author(self, author):
        self.authors.delete(author)
