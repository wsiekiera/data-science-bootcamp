from siekiera_wojciech.zjazd2.OOP_books_solution.person import Person


class Customer(Person):
    def __init__(self, name, surname, date_of_birth):
        super().__init__(name, surname, date_of_birth)
        self.book_entities = []

        # entity = book_ref, price, amount

    def add_book(self, book, quantity):
        self.book_entities.append((book, book.price, quantity))
        book.items_number -= quantity
