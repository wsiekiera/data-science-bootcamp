from siekiera_wojciech.zjazd2.OOP_books_solution.person import Person


class Author(Person):
    def __init__(self, name, surname, date_of_birth, books_list: []):
        super().__init__(name, surname, date_of_birth)
        self.books_list = books_list

    def add_book(self, book):
        self.books_list.append(book)
        book.add_author(self)
