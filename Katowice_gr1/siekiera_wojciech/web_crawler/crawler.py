from bs4 import BeautifulSoup
import requests
from urllib.parse import urlparse
from collections import Counter


def convert_str_to_google_search_phrase(phrase):
    return phrase.replace(" ", "+")


phrases = ["Mistrzostwa Świata w Piłce Nożnej 2018",
           "Avicii",
           "Wybory samorządowe 2018",
           "Tomasz Mackiewicz",
           "About You",
           "Kora Jackowska",
           "Korona królów",
           "Kler",
           "Nanga Parbat",
           "Meghan Markle"]

domain_counter = dict()
domain_list = list()

for phrase in phrases:
    r = requests.get(f"http://google.com/search?q={convert_str_to_google_search_phrase(phrase)}")
    soup = BeautifulSoup(r.text, features="html.parser")
    for cite in soup.select('div.g > h3.r > a'):
        domain_name = urlparse(cite.get("href")[7:]).hostname
        domain_list.append(domain_name)
        if domain_name not in domain_counter:
            domain_counter[domain_name] = 1
        else:
            domain_counter[domain_name] += 1

print(domain_counter)
print(Counter(domain_list))
