from math import factorial

# task 1 (imperative)
words = "Ala ma kota i wszy"
words_split = words.split()
word_letters_count = list()
for single_word in words_split:
    word_letters_count.append(len(single_word))
print(word_letters_count)

# task 2 (comprehension)
word_letters_count_new = [len(single_word) for single_word in words.split()]
print(word_letters_count_new)

# task 3 dict comprehension
countries = {"Poland": "Warsaw", "Germany": "Berlin", "France": "Paris"}
countries_reversed = {value: key for (key, value) in countries.items()}
print(countries_reversed)

# task 4
countries = {"Poland": "Warsaw", "Germany": "Berlin", "France": "Berlin"}
countries_reversed = {value: key for (key, value) in countries.items()}
print(countries_reversed)

# task 5
range(1, 21)
print(list(len(str(factorial(x))) for x in range(1, 1001)))


# task 6 (closure)
def sentence(name):
    name_upper = name.upper()

    def show_sentence():
        return f"Mam na imie {name_upper}"

    return show_sentence


sen1 = sentence("aaa")
sen1()


# task 7 (generator with closure)
def gen(list):
    i = 0

    def next():
        nonlocal i
        element = list[i]
        i += 1
        return element
    return next

gen1 = gen([1,2,3,4,5])
print(gen1())
print(gen1())
print(gen1())



# task 8 (iterable)

iterable = ['spring', 'summer', 'autumn', 'winter']
iterator = iterable.__iter__()
# or iterator = iter(iterable)
print(next(iterator))
print(next(iterator))
print(next(iterator))
print(next(iterator))
# print(next(iterator))
# task 9 generator

milion_squares = (x*x for x in range(1,100000001))

print(list(milion_squares))


# task 9 (generator)

def infinite_generator():
    while True:
        yield 1
        yield 2
        yield 3


# inf = infinite_generator()
# print(next(inf))