from random import randint

from character import Character


class Smok(Character):
    def __init__(self, nazwa, x, y):
        super().__init__(nazwa, x, y)
        self.tekstura = "dragon.png"
        self.punkty_zycia = randint(50, 100)

    def __str__(self):
        return f"Smok: {self.nazwa}, pozycja ({self.x},{self.y}), punkty życia {self.punkty_zycia}"

    def __repr__(self):
        return self.__str__()

    def przyjmij_obrazenia(self, ilosc_obrazen):
        if self.zywy:
            print(f"Smok {self.nazwa}[{self.punkty_zycia}] otrzymuje {ilosc_obrazen} obrażeń.")
            self.punkty_zycia -= ilosc_obrazen
            print(f"Zostało mu {self.punkty_zycia if self.punkty_zycia>=0 else 0} punktów życia.")
            if self.punkty_zycia <= 0:
                self.umrzyj()
        else:
            print(f"Martwe smoki nie przyjmują obrażeń!")

    def zadaj_obrazenia(self, cel, ilosc_obrazen):
        if cel.zywy:
            print(f"Zadaję {ilosc_obrazen} smokowi {cel.nazwa}")
            cel.przyjmij_obrazenia(ilosc_obrazen)
        else:
            print("Cel jest martwy. Nie zadaję obrażeń!")

    def ustaw(self, x, y):
        self.x = x
        self.y = y

    def przesun(self, kierunek, odleglosc):
        if kierunek == "G":
            self.y -= odleglosc
        elif kierunek == "D":
            self.y += odleglosc
        elif kierunek == "L":
            self.x -= odleglosc
        elif kierunek == "P":
            self.x += odleglosc
        else:
            raise Exception('Zły kierunek! Dozwolone kierunki to: G, D, L, P')

    def umrzyj(self):
        self.zywy = False
        self.tekstura = "dragon-dead.png"
        self.punkty_zycia = 0
        print(f"{self.nazwa} is dead")
        print(f"Smok wyrzucił {random.randint(1, 100)} złota")
