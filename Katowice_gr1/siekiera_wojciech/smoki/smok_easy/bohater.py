from random import randint

from character import Character


class bohater(Character):
    def __init__(self, nazwa, x, y, klasa="Warrior"):
        super().__init__(nazwa, x, y)
        self.punkty_zycia = randint(200, 250)
        self.klasa = klasa

    def zadaj_losowe_obrazenia(self):
        return randint(1, 15)
