
from random import randint
from smok_easy.smok import Smok


class SmokMedium(Smok):
    """
    >>> smok=SmokMedium("testowy", 0, 0)
    >>> smok.ustaw(2000, 2000)
    >>> print(smok.x)
    1024
    >>> print(smok.y)
    768
    >>> smok.ustaw(-100, -200)
    >>> print(smok.x)
    0
    >>> print(smok.y)
    0
    >>> smok.przesun("G", 50000)
    >>> smok.przesun("L", 5000)
    >>> print(f"{smok.x} {smok.y}")
    0 0
    """
    SZEROKOSC_EKRANU = 1024
    WYSOKOSC_EKRANU = 768

    def powrot_na_ekran(self):
        if self.x < 0:
            self.x = 0
        elif self.x > SmokMedium.SZEROKOSC_EKRANU:
            self.x = SmokMedium.SZEROKOSC_EKRANU
        if self.y < 0:
            self.y = 0
        elif self.y > SmokMedium.WYSOKOSC_EKRANU:
            self.y = SmokMedium.WYSOKOSC_EKRANU

    def przesun(self, kierunek, odleglosc):
        super().przesun(kierunek, odleglosc)
        self.powrot_na_ekran()

    def ustaw(self, x, y):
        super().ustaw(x, y)
        self.powrot_na_ekran()

    def __init__(self, nazwa, x, y):
        super().__init__(nazwa, x, y)
        self.powrot_na_ekran()
        self.punkty_zycia = randint(100, 150)
