from smok_easy.smok import Smok
# Stwórz smoka w pozycji x=50, y=120 i nazwij go Wawelski
smok = Smok("Wawelski", 50, 120)
print(smok)
# Ustaw nową pozycję na x=10, y=20
smok.ustaw(10, 20)
# Przesuń smoka o 10 w lewo i 20 w dół
smok.przesun("L", 10)
smok.przesun("D", 20)
# Przesuń smoka o 10 w lewo i 15 w prawo
smok.przesun("L", 10)
smok.przesun("P", 15)
# Przesuń smoka o 15 w prawo i 5 w górę
smok.przesun("P", 15)
smok.przesun("G", 5)
# Przesuń smoka o 5 w dół
smok.przesun("D", 5)
# Zadaj 10 obrażeń smokowi
smok.przyjmij_obrazenia(10)
# Zadaj 5 obrażeń smokowi
smok.przyjmij_obrazenia(5)
# Zadaj 3 obrażeń smokowi
smok.przyjmij_obrazenia(30)
# Zadaj 2 obrażeń smokowi
smok.przyjmij_obrazenia(2)
# Zadaj 15 obrażeń smokowi
smok.przyjmij_obrazenia(15)
# Zadaj 25 obrażeń smokowi
smok.przyjmij_obrazenia(25)
# Zadaj 75 obrażeń smokowi
smok.przyjmij_obrazenia(75)



