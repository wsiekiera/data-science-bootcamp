# Dany jest tekst przemównienia John F. Kennedy’ego “Moon Speech” wygłoszony na Rice Stadium
speech = 'We choose to go to the Moon. We choose to go to the Moon in this decade and do the other things. Not ' \
         'because they are easy, but because they are hard. Because that goal will serve to organize and measure the ' \
         'best of our energies and skills. Because that challenge is one that we are willing to accept. One we are ' \
         'unwilling to postpone. And one we intend to win'

map_sentences_wordcount = dict()
word_sum = 0
char_sum = 0

# Dla każdego zdania (zdania oddzielone są kropkami)
sentences = speech.split('.')


# Za pomocą funkcji len() policz ile jest wyrazów
for single_sentence in sentences:
    single_sentence = single_sentence.strip()
    slowa = single_sentence.split(" ")
    word_sum += len(slowa)
    char_sum += len(single_sentence)
    map_sentences_wordcount[single_sentence] = len(slowa)

# Wypisz na ekranie listę słowników o strukturze: zdanie (klucz) -> ilość wyrazów (wartość)
# Na końcu wypisz także ile jest zdań oraz ile słów i znaków naliczyliśmy łącznie
print(map_sentences_wordcount)
print(f'Ilosc zdan={len(sentences)}. Ilosc slow={word_sum}. Ilosc znakow={char_sum}')
