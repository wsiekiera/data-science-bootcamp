tuple_scale = (2, 3, 3.5, 4, 4.5, 5)
list_scale = [float(x) for x in tuple_scale]
print(list_scale)

mark_logger = list()

while True:
    user_input = input('Enter mark ')
    if not user_input:
        break
    if float(user_input) in list_scale:
        mark_logger.append(float(user_input))
average = sum(mark_logger)/len(mark_logger)
print(f'Average={average}')
