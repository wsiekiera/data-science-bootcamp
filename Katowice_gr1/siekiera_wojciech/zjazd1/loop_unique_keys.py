# Mając bazę danych z listingu poniżej
# Wygeneruj listę unikalnych kluczy dictów


DATABASE = [
    {'last_name': 'Jiménez'},
    {'first_name': 'Mark', 'last_name': 'Watney'},
    {'first_name': 'Иван', 'age': 42},
    {'first_name': 'Matt', 'last_name': 'Kowalski', 'born': 1961},
    {'first_name': 'José', 'born': 1961, 'agency': 'NASA'},
]

unique_keys = set()

for single_record in DATABASE:
    for single_key in single_record.keys():
        unique_keys.add(single_key)
print(unique_keys)

