def aviation_numbers(number: (int, float)) -> str:
    alphabet = {'0': 'Zero',
                '1': 'One',
                '2': 'Two',
                '3': 'Tree',
                '4': 'Fower',
                '5': 'Fife',
                '6': 'Six',
                '7': 'Seven',
                '8': 'Ait',
                '9': 'Niner'}

    number_as_str = str(int(number))
    output: str = ''
    for character in number_as_str:
        output += alphabet.get(character)
        output += ' '
    return output


user_input = input('Enter number to convert: ')
print(aviation_numbers(float(user_input)))
