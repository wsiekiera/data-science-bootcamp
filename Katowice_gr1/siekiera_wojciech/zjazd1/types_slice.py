a = 'UL. Jana III Sobieskiego 1/2'
b = 'ulica Jana III Sobieskiego 1/2'
c = 'os. Jana III Sobieskiego 1/2'
d = 'plac Jana III Sobieskiego 1/2'
e = 'aleja Jana III Sobieskiego 1/2'
f = 'alei Jana III Sobieskiego 1/2'
g = 'Jana III Sobieskiego 1 m. 2'
h = 'os. Jana III Sobieskiego 1 apt 2'

sample = 'Jana III Sobieskiego'


def generic_slicer(string_to_slice: str, start_at_char: int) -> str:
    return string_to_slice[start_at_char:start_at_char + len(sample)]


print(generic_slicer(a, 4))
print(generic_slicer(b, 6))
print(generic_slicer(c, 4))
print(generic_slicer(d, 5))
print(generic_slicer(e, 6))
print(generic_slicer(f, 5))
print(generic_slicer(g, 0))
print(generic_slicer(h, 4))
