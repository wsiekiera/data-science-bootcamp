user_input: int = 3177


def meters_to_kilometers(input_number: int) -> int:
    return int(input_number/1000)


def meters_to_miles(input_number: int) -> float:
    return float(input_number/1608)


def meters_to_nautical_miles(input_number: int) -> float:
    return float(input_number/1852)


print(f'Meters: {user_input}')                    # int
print(f'Kilometers: {meters_to_kilometers(user_input)}')                # int
print(f'Miles: {meters_to_miles(user_input)}')                     # float
print(f'Nautical Miles: {meters_to_nautical_miles(user_input)}')            # float
print(f'All: {user_input}, {meters_to_kilometers(user_input)},'
      f'{meters_to_miles(user_input)}, {meters_to_nautical_miles(user_input)}')
