a = ' 1/2'
b = 'ul Jana III Sobieskiego 1/2'
c = 'ul. Jana III Sobieskiego 1/2'
d = 'ul.Jana III Sobieskiego 1/2'
e = 'ulicaJana III Sobieskiego 1/2'
f = 'Ul. Jana III Sobieskiego 1/2'
g = 'UL. Jana III Sobieskiego 1/2'
h = 'ulica Jana III Sobieskiego 1/2'
i = 'Ulica. Jana III Sobieskiego 1/2'
j = 'Jana 3 Sobieskiego 1/2'
k = 'Jana III Sobieskiego 1 m. 2'
l = 'Jana III Sobieskiego 1 apt 2'


def generic_string_cleaner(string_to_clean: str) -> str:
    return string_to_clean.lower()\
        .strip('ul').strip('.').strip('ica').strip('.')\
        .strip(' 1/2').strip('1 m.').strip(' 1 apt')\
        .replace('3', 'iii').title().replace('Iii', 'III').strip()


print(generic_string_cleaner(a))
print(generic_string_cleaner(b))
print(generic_string_cleaner(c))
print(generic_string_cleaner(d))
print(generic_string_cleaner(e))
print(generic_string_cleaner(f))
print(generic_string_cleaner(g))
print(generic_string_cleaner(h))
print(generic_string_cleaner(i))
print(generic_string_cleaner(j))
print(generic_string_cleaner(k))
print(generic_string_cleaner(l))
