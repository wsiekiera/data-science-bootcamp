import re

file = open("files/moon-speech.html")
regex = r"We choose to go to the moon. [a-zA-Z ,.]+"
test_str = file.read()
matches = re.finditer(regex, test_str, re.MULTILINE)

for matchNum, match in enumerate(matches):
    matchNum = matchNum + 1

    print(f"{match.group().strip()}")

file.close()
