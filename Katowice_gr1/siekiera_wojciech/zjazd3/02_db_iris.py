import sqlite3
import datetime

DATABASE = "files/iris.sqlite3"
DATA_FILE = "files/data_input.txt"

dict_list = list()
species_dict = {0: "setosa",
                1: "versicolor",
                2: "virginica"}

with open(DATA_FILE) as data_file:
    for single_line in data_file:
        measurments = single_line.split(",")
        sepal_length = measurments[0]
        sepal_width = measurments[1]
        petal_length = measurments[2]
        petal_width = measurments[3]
        species = species_dict[int(measurments[4])]
        date_time = datetime.datetime.now()
        dict_list.append(
            {"species": species, "sepal_length": sepal_length, "sepal_width": sepal_width, "petal_length": petal_length,
             "petal_width": petal_width, "datetime": datetime.datetime.now()})

with sqlite3.connect(DATABASE) as db:
    # Remove data from DB
    db.execute('DROP TABLE iris')
    # Prepare data structure
    db.execute("""CREATE TABLE IF NOT EXISTS iris (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        species VARCHAR(255),
        datetime DATETIME,
        sepal_length FLOAT,
        sepal_width FLOAT,
        petal_length FLOAT,
        petal_width FLOAT);""")
    # Create index
    db.execute('CREATE INDEX IF NOT EXISTS datetime_index ON iris (datetime);')
    # Insert data
    db.executemany(
        'INSERT INTO iris (species, datetime, sepal_length, sepal_width, petal_length, petal_width) VALUES (:species,'
        ':datetime, :sepal_length, :sepal_width, :petal_length, :petal_width)', dict_list)
    # Retrieve data from DB
    db.row_factory = sqlite3.Row
    for row in db.execute('SELECT * FROM iris'):
        print(dict(row))
