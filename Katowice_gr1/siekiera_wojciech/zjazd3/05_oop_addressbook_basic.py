class AddressBook:
    def __init__(self, contact=[]):
        self.contact = contact

    def add_contact(self, contact):
        self.contact.append(contact)

    def __str__(self):
        to_return = ""
        for single_contact in self.contact:
            to_return += str(single_contact) + "\n\n"
        return to_return


class Address:
    def __init__(self, postcode, city, street, number, country):
        self.postcode = postcode
        self.city = city
        self.street = street
        self.number = number
        self.country = country

    def __str__(self):
        return f"{self.street} {self.number} {self.postcode} {self.city} {self.country}"


class Contact:
    def __init__(self, name, surname, address=[]):
        self.name = name
        self.surname = surname
        self.address = address

    def add_address(self, address):
        self.address.append(address)

    def __str__(self):
        to_return = f"{self.name} {self.surname}\n"
        for single_address in self.address:
            to_return += str(single_address) + "\n"
        return f"{to_return}"


jose = Contact(name="Jose", surname="Jimenez")
address1 = Address(postcode="77058", city="Houston", street="E NASA Pkwy", country="USA", number="12")
address2 = Address(postcode="32899", city="Houston", street="Kenedy Space Center", country="USA", number="33")

jose.add_address(address1)
jose.add_address(address2)

address_book = AddressBook()
address_book.add_contact(jose)

print(address_book)
