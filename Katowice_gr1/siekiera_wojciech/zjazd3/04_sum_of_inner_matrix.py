import numpy as np


random_matrix = np.random.randint(low=10, high=100, size=(16, 16)).astype(float)

print (random_matrix)


transpozed_matrix = random_matrix.transpose()
print(transpozed_matrix)

inner = transpozed_matrix[6:10, 6:10]
print(inner)

sum = inner.sum()
print(sum)