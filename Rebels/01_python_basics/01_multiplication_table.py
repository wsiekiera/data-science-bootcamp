def multiplication_table(width: int, height: int):
    """
        >>> multiplication_table(4,4)
        [1, 2, 3, 4]
        [2, 4, 6, 8]
        [3, 6, 9, 12]
        [4, 8, 12, 16]

        >>> multiplication_table(1, 2)
        [1]
        [2]

        >>> multiplication_table(3, 4)
        [1, 2, 3]
        [2, 4, 6]
        [3, 6, 9]
        [4, 8, 12]
    """
    table = []
    # TODO
    # Implement me!
