"""
Zwróc najwyżej punktowane słowo z podanych. Każdej literze przyporządkowana jest liczba punktów w nastepujący sposób:
a=1, b=2, c=3, d=4 itd
Funkcja na wejściu dostaje listę stringów. Ma zwrócić listę zawierającą stringi dla których suma punktów dla poszczególnych liter
jest największa. Przykładowo:
   >>> highest_score(["abc","def","ghi"])
   ['ghi']
dlatego, że:
"abc" = 1+2+3 = 6
"def" = 4 + 5 + 6 = 15
"ghi" = 7 + 8 + 9 = 24

Na wejściu nie będzie polskich znaków. Gdy jest więcej niż 1 słowo z max punktacją na liście - zwracamy wszystkie.
Duże i małe litery punktujemy tak samo.
"""


def highest_score(words: []) -> []:
    """
    >>> highest_score(["abc","def","ghi"])
    ['ghi']
    >>> highest_score(["aa", "bb", "cc"])
    ['cc']
    >>> highest_score(["abc"])
    ['abc']
    >>> highest_score(["aaa", "c", "b", "ba"])
    ['aaa', 'c', 'ba']
    """
    return list(dict(filter(lambda item: item[1] == max({single_word: sum([{chr(key): key - 96 for key in range(ord('a'), ord('z') + 1)}[single_char] for single_char in single_word]) for single_word in words}.values()), {single_word: sum([{chr(key): key - 96 for key in range(ord('a'), ord('z') + 1)}[single_char] for single_char in single_word]) for single_word in words}.items())).keys())
