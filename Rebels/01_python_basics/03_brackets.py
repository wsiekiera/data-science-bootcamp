"""
Sprawdz czy nawiasy podane w stringu są poprawne. Przykłady:
>>> brackets("()[]([])")
True
>>> brackets("[(])")
False
>>> brackets("[[[((()))}}}[](((())))")
False
>>> brackets("[[[((()))]]][](((())))")
True
"""


def brackets(input_string: str) -> bool:
    pass
